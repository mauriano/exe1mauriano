#ifndef PESSOAFISICA_H
#define PESSOAFISICA_H
#include <Cliente.h>
#include <iostream>

using namespace std;

class PessoaFisica: public Cliente
{
    public:

        string getCpf();
        void setCpf(string c);

        PessoaFisica(string c, string n, string e, string t);
        PessoaFisica();
        void imprimirDados();
        virtual ~PessoaFisica();

    protected:

    private:
        string cpf;
};

#endif // PESSOAFISICA_H
