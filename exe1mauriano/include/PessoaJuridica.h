#ifndef PESSOAJURIDICA_H
#define PESSOAJURIDICA_H
#include <Cliente.h>
#include <iostream>

using namespace std;

class PessoaJuridica: public Cliente
{
    public:

        void imprimirDados();

        string getCnpj();
        string getNomeFantasia();

        void setCnpj(string c);
        void setNomeFantasia(string n);
        PessoaJuridica(string c, string nf, string n, string e, string t);
        PessoaJuridica();
        virtual ~PessoaJuridica();

    protected:

    private:
        string cnpj;
        string nomeFantasia;
};

#endif // PESSOAJURIDICA_H
