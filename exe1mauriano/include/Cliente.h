#ifndef CLIENTE_H
#define CLIENTE_H
#include <iostream>

using namespace std;

class Cliente
{
    public:

        string getNome();
        string getEndereco();
        string getTelefone();

        void setNome(string n);
        void setEndereco(string e);
        void setTelefone(string t);

        virtual void imprimirDados();

        Cliente(string n, string e, string t);
        Cliente();
        virtual ~Cliente();

    protected:

    private:
        string nome;
        string endereco;
        string telefone;
};

#endif // CLIENTE_H
