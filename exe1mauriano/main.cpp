#include <iostream>
#include<Cliente.h>
#include<PessoaFisica.h>
#include<PessoaJuridica.h>


using namespace std;

int main()
{
    Cliente *a = new PessoaFisica("0123032123","Pessoa fisica","endereco pessoa fisica","9988119922");
    a->imprimirDados();

    Cliente *b = new PessoaJuridica("0123032123", "Pessoa juridica ltda.","Pessoa juridica","endereco pessoa juridica","7788119922");
    b->imprimirDados();

    return 0;
}
