#include "PessoaFisica.h"


string PessoaFisica::getCpf(){
    return cpf;
}
void PessoaFisica::setCpf(string c){
    cpf = c;
}

PessoaFisica::PessoaFisica(string c, string n, string e, string t){
    setCpf(c);
    Cliente::setNome(n);
    Cliente::setEndereco(e);
    Cliente::setTelefone(t);
}

PessoaFisica::PessoaFisica()
{
    PessoaFisica("", "", "", "");
}

PessoaFisica::~PessoaFisica()
{
    //dtor
}

void PessoaFisica::imprimirDados(){
    cout << "Nome: " << Cliente::getNome() << endl;
    cout << "CPF: " << getCpf() << endl;
    cout << "Endereco: " << Cliente::getEndereco() << endl;
    cout << "Telefone: " << Cliente::getTelefone() << endl;
}
