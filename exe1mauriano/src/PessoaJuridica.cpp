#include "PessoaJuridica.h"

string PessoaJuridica::getCnpj(){
    return cnpj;
}
string PessoaJuridica::getNomeFantasia(){
    return nomeFantasia;
}

void PessoaJuridica::setCnpj(string c){
    cnpj = c;
}

void PessoaJuridica::setNomeFantasia(string n){
    nomeFantasia = n;
}

PessoaJuridica::PessoaJuridica(string c, string nf, string n, string e, string t){
    setNomeFantasia(nf);
    setCnpj(c);
    Cliente::setNome(n);
    Cliente::setEndereco(e);
    Cliente::setTelefone(t);
}

PessoaJuridica::PessoaJuridica()
{
    PessoaJuridica("","","","","");
}

PessoaJuridica::~PessoaJuridica()
{
    //dtor
}

void PessoaJuridica::imprimirDados(){
    cout << "Nome: " << Cliente::getNome() << endl;
    cout << "Nome Fantasia: " << getNomeFantasia() << endl;
    cout << "CNPJ: " << getCnpj() << endl;
    cout << "Endereco: " << Cliente::getEndereco() << endl;
    cout << "Telefone: " << Cliente::getTelefone() << endl;
}
