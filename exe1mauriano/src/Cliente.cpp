#include "Cliente.h"


string Cliente::getNome(){
    return nome;
}
string Cliente::getEndereco(){
    return endereco;
}
string Cliente::getTelefone(){
    return telefone;
}

void Cliente::setNome(string n){
    nome = n;
}
void Cliente::setEndereco(string e){
    endereco = e;
}
void Cliente::setTelefone(string t){
    telefone = t;
}

Cliente::Cliente(string n, string e, string t)
{
    setNome(n);
    setEndereco(e);
    setTelefone(t);
}

Cliente::Cliente()
{
    Cliente("","","");
}

Cliente::~Cliente()
{
    //dtor
}

void Cliente::imprimirDados(){

}

